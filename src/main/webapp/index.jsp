<%@ page language="java" contentType="text/html; charset=ISO-8859-1" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Welcome</title>
    <style>
      body {
        background-color: #FFF;
        justify-content: center;
        padding: 10%
      }
      div,
      p,
      a {
        text-align: center;
      }
    </style>
  </head>

  <body>
    <div class="content">
      <h1>Simple Interest Calculator</h1>
      
      <a href="<%= request.getContextPath() %>/helloservlet"
        	role="button">
        	<button>Press to enter P, R and T</button>  
      </a>
    </div>
  </body>
</html>