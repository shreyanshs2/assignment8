<%@ page language="java" contentType="text/html; charset=ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="com.example.SimpleInterest" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Simple Interest Calculator</title>
    
    
  </head>
  <%
    double principal = 0.0;    double rate = 0.0;    int time = 0;
    if (request.getMethod().equals("POST")) {
        principal = Double.parseDouble(request.getParameter("principal"));
        rate = Double.parseDouble(request.getParameter("rate"));
        time = Integer.parseInt(request.getParameter("time"));
    }
    SimpleInterest simpleInterest = new SimpleInterest();
    double interestAmount = simpleInterest.getSimpleInterest(principal, rate, time);
    double totalAmount = interestAmount + principal;
   %>
  <body>
    <form action="helloservlet" method = "post">
      <div >
        <div>
          <div >
            <label for="principal">Principal</label>
            <input name = "principal" type="number" id="principal" value="<%= principal %>" />
          </div>
          <div >
            <label for="rate">Rate:</label>
            <input name = "rate" type="number" id="rate" value="<%= rate %>" placeholder="0"/>
          </div>
        </div>
        <label for="time">Time:</label>
        <div >
          <input name = "time" type="number" id="time" value="<%= time %>" />
        </div>
        <input type="submit" id="calculate-btn" value="Calculate" />
        <div id="result">
          <div>Principal = <span><%= principal %></span></div>
          <div>Interest = <span><%= interestAmount %></span></div>
          <div>Total Amount = <span><%= totalAmount %></span></div>
        </div>
      </div>
    </form>
  </body>
</html>