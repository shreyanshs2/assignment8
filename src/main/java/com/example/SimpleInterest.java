package com.example;

public class SimpleInterest {

	public double getSimpleInterest(double principal, double rate, double time) {
		return (principal * rate * time) / 100;
	}

}
